shyft package
=============

Subpackages
-----------

.. toctree::

    shyft.time_series
    shyft.api
    shyft.orchestration

Submodules
----------

shyft.version module
--------------------

.. automodule:: shyft.version
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: shyft
    :members:
    :undoc-members:
    :show-inheritance:
