**************************
What is the hydrological region_model
**************************

The hydrological region-model consists of cells, where each cell have a "personality" that models the geo-physical properties that best fits that part of the model.
Each cell have a catchment-identifier, that allows the cells to be summed together in order to provide catchment scale computations.
At region-level, user can set the method-stack-parameter for each catchment-id. Technically, this can be done on each cell as well.
Each cell can optionally be connected to a river-model, where the response from the cell to the first river can be shaped by a convolution mask (u-hydrogram).
Rivers get their input from the connected cells, as well as upstream river-segments, allowing routing to take place, of the sum-flow that is shaped and delayed using a convolution mask.

The region-model provides methods to interpolate/project external forcing data, like temperature, precipitation etc. on to the cells taking the cell-distance/height etc. into account.

The region-model provides means of parameter-optimization where the user provides the goal-function, and ranges for selected parameters, and then the optimization routines find the parameters minimizes the value of the goal-function.

The specification of the goal function is very flexible, allowing multiple criteria and period-selective calibration strategies.
